﻿using Abp.Application.Services;
using Template.MultiTenancy.Dto;

namespace Template.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

